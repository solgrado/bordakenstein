
# Bordakenstein

![Captura Bordakenstein](https://solgrado.gitlab.io/recursos/bordakenstein_screenshot.png)

**Bordakenstein** es un programa que, junto con un Arduino, gestiona el control de una bordadora DIY.

### Formatos

Esta versión lee archivos de bordados **DST** (Tajima). También utiliza un formato libre propio llamado "Bordakenstein extension" (BKE).
Puede convertir archivos DST a BKE.

Para más detalles sobre el formato DST consultar: [https://community.kde.org/Projects/Liberty/File_Formats/Tajima_Ternary](https://community.kde.org/Projects/Liberty/File_Formats/Tajima_Ternary)

Para más detalles sobre el formato BKE consultar la documentación del proyecto Bordakenstein: [https://solgrado.gitlab.io/bordakenstein-doc/software/formato-bke.html](https://solgrado.gitlab.io/bordakenstein-doc/software/formato-bke.html)

### Bordados

Los bordados de ejemplo fueron creados expresamente para este proyecto partiendo de imágenes libres.

### Otros recursos

Los sonidos fx fueron generados con el programa **sfxr** [http://www.drpetter.se/project_sfxr.html](http://www.drpetter.se/project_sfxr.html).

### Código y dependencias

Este programa fue creado con Qt-Creator, por lo que depende de algunas librerías Qt (pendiente de actualización):

```
qt5-declarative
qt5-multimedia
qt5-svg
qt5-serialport
```

Para más información sobre su instalación y uso dirígete a la documentación: [https://solgrado.gitlab.io/bordakenstein-doc/](https://solgrado.gitlab.io/bordakenstein-doc/)

## Licencia

Copyright (C) 2017-2021 Santi O.L. (Solgrado) 

Este programa es software libre: puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según se encuentra publicada por la Free Software Foundation, bien de la versión 3 de dicha Licencia o bien (según su elección) de cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General  de GNU junto con este programa. Si no ha sido así, consulte: http://www.gnu.org/licenses/.
