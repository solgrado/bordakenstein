/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "customgraphicsview.h"

#include <QScrollBar>

CustomGraphicsView::CustomGraphicsView(QWidget *parent) : QGraphicsView(parent)
{
    scale(0.5, 0.5);
    //setRenderHints(QPainter::Antialiasing);
    _arrastrar = false;
}

void CustomGraphicsView::ajustarVista()
{
    QRectF area = scene()->itemsBoundingRect() + QMarginsF(10,10,10,10);
    fitInView(area, Qt::KeepAspectRatio);
}

void CustomGraphicsView::wheelEvent(QWheelEvent *event)
{
    QPoint pos  = event->pos();
    QPointF posf = this->mapToScene(pos);
    double angle = event->angleDelta().y();
    double escala = 1.0;

    if (angle > 0) {
        escala = 1 + ( angle / 3600); // sustituido "360 * 0.1" por "3600".
    }
    else if (angle < 0) {
        escala = 1 - (-angle / 3600);
    }
    scale(escala, escala);

    QSize tamWidget(this->viewport()->width(), this->viewport()->height());

    QPointF sf = this->mapToScene(QPoint(tamWidget.width() - 1, tamWidget.height() - 1))
            - this->mapToScene(QPoint(0,0));

    double lf = posf.x() - pos.x() * sf.x() / tamWidget.width();
    double tf = posf.y() - pos.y() * sf.y() / tamWidget.height();

    this->ensureVisible(lf, tf, sf.x(), sf.y(), 0, 0);

    event->accept();
}

void CustomGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton){
        _arrastrar = true;
        _inicioArrastre = event->pos();
        setCursor(Qt::ClosedHandCursor);
        event->accept();
    }
    else {
        event->ignore();
    }
}

void CustomGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton){
        _arrastrar = false;
        setCursor(Qt::ArrowCursor);
        event->accept();
    }
    else {
        event->ignore();
    }
}

void CustomGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if (_arrastrar){
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - _inicioArrastre.x()));
        verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - _inicioArrastre.y()));
        _inicioArrastre = event->pos();
        event->accept();
    }
    else {
        event->ignore();
    }
}
