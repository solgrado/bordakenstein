/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "gbinfo.h"
#include "ui_gbinfo.h"
#include "masinfo.h"

GBInfo::GBInfo(QWidget *parent, Bordado *b) :
    QGroupBox(parent),
    ui(new Ui::GBInfo)
{
    ui->setupUi(this);
    _bordado = b;
}

GBInfo::~GBInfo()
{
    delete ui;
}

void GBInfo::actualizaDimensiones()
{
    int _xIzquierda = 0, _xDerecha = 0, _yArriba = 0, _yAbajo = 0;

    ///Es posible que esté del revés
    for (int i = 0; i < _bordado->tam(); i++){
        QLine l = _bordado->lineaPuntada(i);

        if (-l.x2() < _xIzquierda) {_xIzquierda = -l.x2();}
        else if (-l.x2() > _xDerecha) {_xDerecha = -l.x2();}
        if (l.y2() > _yArriba) {_yArriba = l.y2();}
        else if (l.y2() < _yAbajo) {_yAbajo = l.y2();}
    }

    ui->labelTamIzquierda->setText(QString::number(_xIzquierda / 10.0));
    ui->labelTamDerecha->setText(QString::number(_xDerecha / 10.0));
    ui->labelTamArriba->setText(QString::number(_yArriba / 10.0));
    ui->labelTamAbajo->setText(QString::number(_yAbajo / 10.0));
    ui->labelAnchoDibujo->setText(QString::number((-_xIzquierda + _xDerecha) / 10.0));
    ui->labelAltoDibujo->setText(QString::number((_yArriba - _yAbajo) / 10.0));

    ui->labelNumPuntadas->setText(QString::number(_bordado->tam() - 1));
    ui->labelNumCambiosColor->setText(QString::number(_bordado->numCambios()));
}

void GBInfo::on_pButtonMasInfo_clicked()
{
    if (!MasInfo::estaInstanciada()){
        MasInfo *masinfo = new MasInfo(_bordado);
        masinfo->setAttribute((Qt::WA_DeleteOnClose));
        masinfo->show();
    }
}
