/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef GBINFO_H
#define GBINFO_H

#include <QGroupBox>
#include "bordado.h"

namespace Ui {
class GBInfo;
}

class GBInfo : public QGroupBox
{
    Q_OBJECT

public:
    explicit GBInfo(QWidget *parent, Bordado *b);
    ~GBInfo();

public slots:
    //Actualiza la información del bordado
    void actualizaDimensiones();

private slots:
    void on_pButtonMasInfo_clicked();

private:
    Ui::GBInfo *ui;

    Bordado *_bordado;
};

#endif // GBINFO_H
