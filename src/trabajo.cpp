/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "trabajo.h"

Trabajo::Trabajo(Bordado *b, QObject *parent) : QThread(parent)
{
    _arduinoIsOk = false;
    _pausa = true;
    _bordado = b;
    _trabajoEnCurso = false;

}

void Trabajo::setPausa(bool b)
{
    _pausa = b;
    if (!_pausa){
        _tiempo.restart();
        emit enviar("M");
    }
    else{
        emit enviar("N");
    }
}

void Trabajo::detener()
{
    setPausa(true);
    contadorPuntadas = 0;
    _trabajoEnCurso = false;

    emit enviar(_bordado->textoAPuerto(contadorPuntadas));
}

void Trabajo::_getTiempo()
{
    //Actualiza el tiempo restante cada diez puntadas.
    if (contadorPuntadas % 10 == 0){
        int t = _tiempo.msecsTo(QTime::currentTime()) * ((_bordado->tam() - contadorPuntadas) / 10);
        QTime temp(0, 0, 0, 0);
        temp = temp.addMSecs(t);
        _tiempo.restart();

        emit setTiempo(temp.toString("hh:mm:ss"));
    }
}

void Trabajo::run()
{
    _trabajoEnCurso = true; //Se repite innecesariamente

    if(_arduinoIsOk && _dibujoCompleto && contadorPuntadas < _bordado->tam() && !_pausa) {

        _dibujoCompleto = false;
        _arduinoIsOk = false;
        emit enviar(_bordado->textoAPuerto(contadorPuntadas));
        emit dibujarPuntada(contadorPuntadas);
        emit actualizaProgreso(contadorPuntadas);

        switch (_bordado->getPuntada(contadorPuntadas).flag) {
        case STOP:
            emit enviar("N");
            setPausa(true);
            emit cambiarHilo();
            break;
        case END:
            setPausa(true);
            _trabajoEnCurso = false;

            emit final();
            break;
        default:
            break;
        }

        contadorPuntadas++;
        _getTiempo();
    }
}

