/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "customscene.h"
#include "bordado.h"

CustomScene::CustomScene(QObject *parent, Bordado *b) : QGraphicsScene(parent)
{
    _bordado = b;

    _bastidor = new QGraphicsEllipseItem;
    _bastidor->setPen(QPen(QBrush(Qt::darkYellow), 3, Qt::DashLine, Qt::RoundCap));
    _limite = new QGraphicsEllipseItem;
    _limite->setPen(QPen(QBrush(Qt::red), 3, Qt::DashLine, Qt::RoundCap));
    _mirilla = new QGraphicsSvgItem(QString(":/iconos/mirilla.svg"));
    _mirilla->setPos(-16, -16);
    _mirilla->setZValue(1.0);

    //Estos items están siempre presentes, se redimensionan o se ocultan pero nunca se eliminan.
    addItem(_bastidor);
    addItem(_limite);
    addItem(_mirilla);

    pintarBastidor("180");///////////////////// siempre 180?
    setRejillaVisible(false);
}

void CustomScene::visualizarHasta(int n)
{
    int i = _bordado->getLimiteColor(n);
    estaBordado(0, n);
    aBordar(n, i);
    pendienteBordar(i, _bordado->tam());
    setPosMirilla(_bordado->puntoPuntada(n));
}

void CustomScene::dibujarRejilla(const int tamBastidor)
{
    //Borrar rejilla si la hubiera
    for (int i = 0; i < _rejilla.size(); i++){
        removeItem(_rejilla.at(i));
    }
    _rejilla.clear();

    //Dibujar rejilla
    QPen penRejilla(QBrush(Qt::gray), 1.0);
    int extremoRejilla = tamBastidor * 5;
    int distanciaRejilla = 100;
    for (int i = -extremoRejilla; i <= extremoRejilla; i+= distanciaRejilla){
        _rejilla.append(addLine(-extremoRejilla, i, extremoRejilla, i, penRejilla));
        _rejilla.append(addLine(i, -extremoRejilla, i, extremoRejilla, penRejilla));
    }
    setRejillaVisible(_rejillaEsVisible);
}

void CustomScene::setRejillaVisible(bool b)
{
    for (int i= 0; i < _rejilla.size(); i++){
        _rejilla.at(i)->setVisible(b);
    }
    _rejillaEsVisible = b;
}

void CustomScene::pintarBastidor(QString tamBastidor)
{
    int tam = tamBastidor.toInt();
    dibujarRejilla(tam);
    tam *= 10;
    const int inicioBastidor = -tam / 2;
    const int inicioLimite = inicioBastidor + 200;
    const int tamLimite = tam - 400;
    _bastidor->setRect(inicioBastidor, inicioBastidor, tam, tam);
    _limite->setRect(inicioLimite, inicioLimite, tamLimite, tamLimite);
}

void CustomScene::setPosMirilla(QPoint punto)
{
    _mirilla->setPos(-punto.x() -16, punto.y() -16);
}

void CustomScene::setBastidorVisible(bool b)
{
    _bastidor->setVisible(b);
    _limite->setVisible(b);
}

void CustomScene::setMirillaVisible(bool b)
{
    _mirilla->setVisible(b);
}

void CustomScene::importarBordado()
{
    borrarDibujo();
    for (int i = 0; i < _bordado->tam(); i++){
        QLine linea = _bordado->lineaPuntada(i);
        //Volteo gráfico horizontal porque graphicsview funciona con coordenadas invertidas
        linea.setP1(QPoint(-linea.p1().rx(), linea.p1().ry()));
        linea.setP2(QPoint(-linea.p2().rx(), linea.p2().ry()));

        _listaItemLineas.append(addLine(linea, QPen(_bordado->colorPuntada(i))));

        _listaItemLineas.last()->setOpacity(0.1);
        _listaItemLineas.last()->setZValue(0.0);
    }
    aBordar(0, _bordado->puntadaFinDeColor(1));
    setPosMirilla(QPoint(0, 0));
}

void CustomScene::pintarLinea(int i)
{
    QPoint punto = _bordado->puntoPuntada(i);
    _listaItemLineas.at(i)->setPen(QPen(_bordado->colorPuntada(i)));
    _listaItemLineas.at(i)->setOpacity(1.0);
    setPosMirilla(punto);

    emit seHaPintado(i);
}

void CustomScene::borrarDibujo()
{
    for (int i = 0; i < _listaItemLineas.size(); i++){
        removeItem(_listaItemLineas.at(i));
    }
    _listaItemLineas.clear();
}

void CustomScene::aBordar(const int inicio, const int fin)
{
    for (int i = inicio; i < fin; i++){
        _listaItemLineas.at(i)->setPen(QPen(Qt::red));
        _listaItemLineas.at(i)->setOpacity(0.8);
    }
}

void CustomScene::pendienteBordar(const int inicio, const int fin)
{
    for (int i = inicio; i < fin; i++){
        _listaItemLineas.at(i)->setPen(QPen(Qt::black));
        _listaItemLineas.at(i)->setOpacity(0.1);
    }
}

void CustomScene::estaBordado(const int inicio, const int fin)
{
    for (int i = inicio; i < fin; i++){
        _listaItemLineas.at(i)->setPen(QPen(Qt::black));
        _listaItemLineas.at(i)->setOpacity(1.0);
    }
}

void CustomScene::reiniciaBordado()
{
    aBordar(0, _bordado->puntadaFinDeColor(1));
    pendienteBordar(_bordado->puntadaInicioDeColor(2), _bordado->tam());
    setPosMirilla(QPoint(0, 0));
}
