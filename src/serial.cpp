/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "serial.h"

#include <QPoint>

Serial::Serial(QObject *parent) : QSerialPort(parent)
{
    setBaudRate   (QSerialPort::Baud115200);
    setDataBits   (QSerialPort::Data8);
    setParity     (QSerialPort::NoParity);
    setStopBits   (QSerialPort::OneStop);
    setFlowControl(QSerialPort::NoFlowControl);

    connect(this, SIGNAL(readyRead()), this, SLOT(limpiaYContinua()));
}

int Serial::conectar(const QString &nombrePuerto)
{
#if defined(Q_OS_LINUX)
    setPortName("/dev/" + nombrePuerto);
#elif defined(Q_OS_WIN)
    setPortName(nombrePuerto);
#endif

    if (open(QIODevice::ReadWrite)){
        return -1;
    }
    else {
        return error(); //funcion qt obsoleta
    }
}

void Serial::desconectar()
{
    if (isOpen()){
        close();
    }
}

void Serial::desplazarA(const QPoint &p)
{
    QByteArray qba;
    qba.append("X" + QString::number(p.x()) +
               "Y" + QString::number(p.x()) +
               "F32\n"); ///Nuevo flag para desplazamientos libres. No implementado en Arduino.
    write(qba);
}

void Serial::onEnviar(QByteArray qba)
{
    write(qba);
}

void Serial::limpiaYContinua()
{
    if (bytesAvailable() > 1){ ///cosa del serial o no
        readAll();
        clear(QSerialPort::Input);
        emit setArduinoOk();
    }
    emit continua();
}

