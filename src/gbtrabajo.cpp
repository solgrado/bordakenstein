﻿/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include <QListWidgetItem>
#include <QMenu>
#include <QLabel>

#include "gbtrabajo.h"
#include "ui_gbtrabajo.h"
#include "mensaje.h"

GBTrabajo::GBTrabajo(QWidget *parent,
                     Serial *puerto,
                     Trabajo *hilo,
                     Bordado *bordado,
                     Simulacion *simulacion) :
    QGroupBox(parent),
    ui(new Ui::GBTrabajo)
{
    ui->setupUi(this);

    _puerto = puerto;
    _trabajo = hilo;
    _bordado = bordado;
    _simulacion = simulacion;
    _numColor = 1;
    _puntadaActual = 0;
    _trabajoEnCurso = false;

    connect(_trabajo, SIGNAL(actualizaProgreso(int)), this, SLOT(actualizarProgreso(int)));
    connect(_trabajo, SIGNAL(final()), this, SLOT(finalizar()));

    //_gbcontrolsim
    _gbcontrolsim = new GBControlSim(ui->groupBoxControlesSimulacion, _simulacion, _bordado);
    connect(_gbcontrolsim, &GBControlSim::pausar, [&](){ui->groupBoxControlesBordar->setEnabled(true);});
    connect(_gbcontrolsim, SIGNAL(detener()), SLOT(detenerSim()));
    connect(_gbcontrolsim, SIGNAL(activarModo()), SLOT(activarModoSim()));
    connect(_gbcontrolsim, SIGNAL(anteriorColor()), SLOT(saltoAColorAnterior()));
    connect(_gbcontrolsim, SIGNAL(siguienteColor()), SLOT(saltoAColorSiguiente()));
    connect(_simulacion, SIGNAL(final()), this, SLOT(finalizar()));

    //_simulacion
    connect(_simulacion, SIGNAL(cambiarHilo()), this, SLOT(cambiarHiloSim()));
    connect(_simulacion, SIGNAL(actualizaProgreso(int)), this, SLOT(actualizarProgreso(int)));

    //Lista de acciones
    ui->listWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->listWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(submenu(QPoint)));

    //THIS - Controles de bordado
    connect(ui->pButtonDetener, &QPushButton::clicked, [&](){detenerTrabajo();});

    ui->tabWidgetBordar->setEnabled(false);
}

GBTrabajo::~GBTrabajo()
{
    delete ui;
}

/** ------------------------------------------------------------------------------
 * ------------------------------------ Ui ---------------------------------------
 * -------------------------------------------------------------------------------*/

void GBTrabajo::activarModoSim()
{
    ui->groupBoxControlesBordar->setEnabled(false);
    emit controlesActivos(false);
}

void GBTrabajo::activarControles()
{
    ui->tabWidgetBordar->setEnabled(true);
    ui->groupBoxControlesBordar->setEnabled(true);
    _gbcontrolsim->setEnabled(true);
    emit controlesActivos(true);
}

void GBTrabajo::on_tabWidgetBordar_currentChanged(int index)
{
    static int tempPuntada = 0;
    static quint8 tempNumColor = 0;

    if (_trabajo->isPausa() && _simulacion->isPausa()){
        if (index == 0){
            _puntadaActual = tempPuntada;
            _numColor = tempNumColor;

            ui->labelColorActual->setText(QString::number(_numColor));
            int proxColor = _bordado->puntadaFinDeColor(_numColor) - _puntadaActual;
            ui->labelProximoColor->setText(QString::number(proxColor));
            ui->listWidget->setCurrentRow(_puntadaActual);

            emit visualizarHasta(_puntadaActual);
            emit progreso(_puntadaActual);
            if (_puntadaActual == 0){
                controlesActivos(true);
            }
        }
        else {
            tempPuntada = _puntadaActual;
            tempNumColor = _numColor;
            _simulacion->setPuntadaActual(_puntadaActual);
        }
    }
}

/** ------------------------------------------------------------------------------
 * ---------------------------------- Trabajo ------------------------------------
 * -------------------------------------------------------------------------------*/

void GBTrabajo::botonesModoTrabajo(bool b)
{
    ui->pButtonIniciar->setDisabled(b);
    ui->pButtonPausar->setEnabled(b);
    ui->pButtonDetener->setDisabled(b);
}

void GBTrabajo::on_pButtonIniciar_clicked()///puede ser continuar!
{
    if (_puerto->isOpen()){
        emit controlesActivos(false);
        _gbcontrolsim->setEnabled(false);

        botonesModoTrabajo(true);

        _puntadaActual = _trabajo->contadorPuntadas;

        if (_puntadaActual != ui->listWidget->currentRow()){
            ui->listWidget->setCurrentRow(_puntadaActual);
            emit visualizarHasta(_puntadaActual);
        }

        _trabajo->setPreInicio();
        _trabajo->start();
    }
    else {
        Mensaje::puertoNoConfigurado();
    }
}

void GBTrabajo::on_pButtonPausar_clicked()
{
    _trabajo->setPausa(true);
    _gbcontrolsim->setEnabled(true);
    botonesModoTrabajo(false);
}

void GBTrabajo::detenerTrabajo()
{
    _trabajo->detener();
    botonesModoTrabajo(false);
    detener();
}

void GBTrabajo::cambiarHilo()
{
    sumaUnHilo();

    if (Mensaje::cambioDeHilo()) {
        _trabajo->setPausa(false);
        _puerto->limpiaYContinua();
    }
}

/** --------------------------------------------------------------------------------
 * ------------------------------ Simulacion ---------------------------------------
 * ---------------------------------------------------------------------------------*/

void GBTrabajo::detenerSim()
{
    //Reinicios
    emit reiniciarBordado();
    ui->listWidget->setCurrentRow(0);
    ui->labelColorActual->setText("1");
    ui->labelProximoColor->setText(QString::number(_bordado->puntadaFinDeColor(1)));

    activarControles();
}

void GBTrabajo::saltoAColorSiguiente()
{
    if (_numColor < _bordado->numCambios() + 1) _numColor++;
    saltarAColor();
}

void GBTrabajo::saltoAColorAnterior()
{
    if (_numColor > 1) _numColor--;
    saltarAColor();
}

void GBTrabajo::saltarAColor()
{
    _puntadaActual = _bordado->puntadaInicioDeColor(_numColor);/////////////
    _simulacion->setPuntadaActual(_puntadaActual);
    ui->labelColorActual->setText(QString::number(_numColor));

    ui->listWidget->setCurrentRow(_puntadaActual);
    int proxColor = _bordado->puntadaFinDeColor(_numColor) - _puntadaActual;
    ui->labelProximoColor->setText(QString::number(proxColor));

    emit progreso(_puntadaActual);
    emit visualizarHasta(_puntadaActual);
}

void GBTrabajo::cambiarHiloSim()
{
    sumaUnHilo();

    if (Mensaje::cambioDeHilo()) {
        _simulacion->start();
    }
}

/** ------------------------------------------------------------------------------
 * ------------------- Funciones comunes a Trabajo y simulación ------------------
 * -------------------------------------------------------------------------------*/

void GBTrabajo::finalizar()
{
    if (Mensaje::trabajoFinalizado()){
        detener();
    }
}

void GBTrabajo::detener()
{
    //Reinicia valores y escena
    _puntadaActual = 0;
    ui->listWidget->setCurrentRow(0);
    _numColor = 1;
    emit reiniciarBordado();

    activarControles();
}

void GBTrabajo::sumaUnHilo()
{
    _numColor++;

    int i = _bordado->puntadaFinDeColor(_numColor) - _puntadaActual;
    ui->labelProximoColor->setText(QString::number(i));
    ui->labelColorActual->setText(QString::number(_numColor));

    emit aBordar(_bordado->puntadaInicioDeColor(_numColor), _bordado->puntadaFinDeColor(_numColor));
}

void GBTrabajo::actualizarProgreso(int i)
{
    _puntadaActual = i;
    ui->listWidget->setCurrentRow(i);
    int proxColor = _bordado->puntadaFinDeColor(_numColor) - _puntadaActual;
    ui->labelProximoColor->setText(QString::number(proxColor));
}

/** ------------------------------------------------------------------------------
 * ---------------------------------- Lista --------------------------------------
 * -------------------------------------------------------------------------------*/

void GBTrabajo::cargarLista()
{
    borrarLista();
    for (int i = 0; i < _bordado->tam(); i++){
        QListWidgetItem *qlwi = new QListWidgetItem(_bordado->textoPuntada(i), ui->listWidget);
        qlwi->setBackground(_bordado->colorLista(i));
    }

    ui->labelColorActual->setText("1");
    _puntadaActual = 0;
    actualizarProgreso(_puntadaActual);
}

void GBTrabajo::borrarLista()
{
    ui->listWidget->clear();
}

bool GBTrabajo::tareaEnCurso()
{
    if (_trabajo->trabajoEnCurso() || _simulacion->simulacionEnCurso()){
        return true;
    }
    else return false;
}

/* ----------------------------- Submenú -------------------------------------*/

//Submenú que aparece al presionar el botón derecho sobre una acción de la lista
void GBTrabajo::submenu(const QPoint &pos)
{
    if (_trabajo->isPausa() && _simulacion->isPausa() && ui->listWidget->count() > 0){

        QPoint posGlobal = ui->listWidget->mapToGlobal(pos);
        QMenu menu;

        if (_puerto->isOpen() && ui->tabWidgetBordar->currentIndex() == 0){
            menu.addAction("Visualizar hasta aquí", this, SLOT(visualizarHastaAqui()));
            menu.addAction("Mover hasta aquí", this, SLOT(desplazarHastaAqui()));
        }
        else if (ui->tabWidgetBordar->currentIndex() != 0){
            menu.addAction("Ir aquí", this, SLOT(visualizarHastaAqui()));
        }
        menu.exec(posGlobal);
    }
}

void GBTrabajo::visualizarHastaAqui()
{
    _puntadaActual = ui->listWidget->currentRow();
    _simulacion->setPuntadaActual(_puntadaActual);
    _numColor = quint8(_bordado->getNumColor(_puntadaActual));

    emit visualizarHasta(_puntadaActual);
    emit mostrarInfo();
}

void GBTrabajo::desplazarHastaAqui()
{
    if (Mensaje::confirmarDesplazamiento()){
        visualizarHastaAqui();
        _puntadaActual = ui->listWidget->currentRow();
        _trabajo->contadorPuntadas = _puntadaActual;
        _puerto->desplazarA(_bordado->puntoPuntada(_puntadaActual));
    }
}
