/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>

#include "customgraphicsview.h"
#include "customscene.h"
#include "importador.h"
#include "serial.h"
#include "gbcontrol.h"
#include "gbcontrolsim.h"
#include "gbconfig.h"
#include "gbinfo.h"
#include "trabajo.h"
#include "gbtrabajo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void mostrarInfoEspecial();
    void activarHerramientas(bool b);
    void onActualizaBarraYTiempo(int i);

private slots:
    void abrirArchivo();
    void abrirEjemplo(QString &ejemplo);
    void guardarComo();
    void setTitulo(QString t){setWindowTitle(t + " - Bordakenstein");}
    void mostrarTiempo(QString);
    void onActualizaProgreso(int i);
    void showPreview();

protected:
    void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    Ui::MainWindow *ui;

    Bordado *_bordado;
    Trabajo *_trabajo;
    Simulacion *_simulacion;
    Importador * _importador;
    GBInfo *_info;
    CustomScene *_escena;
    Serial *_puerto;
    GBConfig *_config;
    GBControl *_control;
    GBTrabajo *_gbTrabajo;
    GBControlSim *_gbcontrolsim;

    int _totalPuntadas;
    int _puntadaActual;
    bool _trabajoEnCurso;

    bool _comprobarTrabajoEnCurso();
    void _abrirBordado();
    void _actualizarDatos();
    void saltarAColor();
};

#endif // MAINWINDOW_H
