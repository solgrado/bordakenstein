/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef TRABAJO_H
#define TRABAJO_H

#include <QThread>
#include <QTime>

#include "bordado.h"

/*
 * Esta clase contiene breves funciones para el trabajo (bordado en bordadora).
 * Lo que empezó siendo la clase más compleja del programa se ha ido reduciendo
 * hasta unas pocas líneas de código con un QThread al que apenas se le saca provecho.
*/

class Trabajo : public QThread
{
    Q_OBJECT
public:
    explicit Trabajo(Bordado *b, QObject *parent = nullptr);

    //Devuelve si está en pausa el bordado
    bool isPausa()const {return _pausa;}

    //Prepara las variables para el inicio del trabajo
    void setPreInicio(){
        _dibujoCompleto = true;
        setPausa(false);
        setArduinoOk();
    }

    //Devuelve si el trabajo ha sido iniciado pero no detenido (ignora las pausas)
    bool trabajoEnCurso()const {return _trabajoEnCurso;}

    //Número de la puntada en la que se encuentra el trabajo
    int contadorPuntadas;

protected:
    void run() override;

signals:
    void actualizaProgreso(int);
    void cambiarHilo();
    void dibujarPuntada(int);
    void setTiempo(QString);
    void enviar(QByteArray);
    void final();

public slots:
    void setPausa(bool b);
    void setArduinoOk() {_arduinoIsOk = true;}
    void detener();
    void onSeHaPintado() {_dibujoCompleto = true;}

private:
    bool _dibujoCompleto;
    bool _arduinoIsOk;
    bool _pausa;
    bool _trabajoEnCurso;

    Bordado *_bordado;
    QTime _tiempo;

    void _getTiempo();
};

#endif // TRABAJO_H
