/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "masinfo.h"
#include "ui_masinfo.h"

MasInfo::MasInfo(Bordado *b, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MasInfo)
{
    ui->setupUi(this);
    _hayInstancia = true;
    _bordado = b;
    ui->labelDescripcionTitulo->setText(_bordado->info.titulo);
    ui->labelDescripcionAutoria->setText(_bordado->info.autoria);
    ui->labelDescripcionFormato->setText(_bordado->info.formato);
    ui->labelDescripcionLicencia->setText(_bordado->info.licencia);
    ui->textBrowserComentario->setText(_bordado->info.comentario);
}

MasInfo::~MasInfo()
{
    _hayInstancia = false;
    delete ui;
}

bool MasInfo::_hayInstancia = false;

bool MasInfo::estaInstanciada()
{
    return _hayInstancia;
}
