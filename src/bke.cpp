/******************************** LICENCIA **********************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

/******************************* FORMATO BKE ********************************
 *
 * BKE (Bordakenstein extension)
 * es un primer boceto muy sencillo de un formato libre para archivos de bordado.
 * Se escribe en texto plano y esta versión no está pensada para tener archivos compactos.
 *
 * --------------------------------------------------------------------------------------
 * LA CABECERA:
 *
 * Puede contener información adicional a modo de comentario que no será interpretada
 * como código de bordado. Todas estas líneas deben ir precedidas del signo #
 *
 * Datos que serán leídos:
 * FORMATO=BKE_00(la numeración irá evolucionando según nuevas versiones. Dato imprescindible)
 * TITULO="Nombre del bordado"
 * AUTORIA="autor/a del bordado"
 * LICENCIA="licencia bajo la cual está el bordado.
 *
 *
 * CUERPO DEL BORDADO:
 *
 * Justo antes del cuerpo del bordado contiene una línea con tres guiones "---" que
 * indican el inicio de las instrucciones.
 *
 * El cuerpo del bordado contiene líneas del siguiente modo:
 * 36,85,0
 * Donde 36 es la coordenada X, 85 es la coordenada Y, 0 es
 * el código de la accion a realizar en dichas coordenadas, que en este caso es dar una puntada.
 *
 * --------------------------------------------------------------------------------------
 * Los códigos de acción, a diferencia de los archivos dst, son números enteros,
 * aunque se utilizan los mismos números para que fuera fácil de recordar. Los implementados son:
 * 0  Puntada
 * 1  Salto
 * 4  Cambio de hilo
 * 16 Fin del programa
 *
 */

#include "bke.h"
#include <QFile>
#include <QDataStream>
#include <QDebug>

Bke::Bke(Bordado *bordado)
{
    _bordado = bordado;
}

int Bke::importar()
{
    _archivo.setFileName(_bordado->nombreArchivo);
    _archivo.open(QIODevice::ReadOnly);

    if (leerCabecera() == 0){ //Version BKE_00
        _bordado->reset();

        _bordado->info.formato = "bke (version 0)";

        while(!_archivo.atEnd()){
            _lineaArchivo = _archivo.readLine();

            int inicioDatoCoordY = _lineaArchivo.indexOf(",");
            int inicioDatoAccion = _lineaArchivo.indexOf(",", inicioDatoCoordY + 1);

            Puntada puntada;
            puntada.x = _lineaArchivo.mid(0, inicioDatoCoordY).toInt();
            puntada.y = _lineaArchivo.mid(inicioDatoCoordY + 1, inicioDatoAccion - 1 - inicioDatoCoordY).toInt();
            puntada.flag = _lineaArchivo.mid(inicioDatoAccion + 1, _lineaArchivo.length() - inicioDatoAccion).toUShort();

            if (puntada.flag == 4 || puntada.flag == 16) {
                _bordado->addCambioColor(_bordado->tam());
            }
            _bordado->addPuntada(puntada);
        }
    }
    else {
        return 1;
    }

    _archivo.close();
    return 0;
}

int Bke::leerCabecera()
{
    _lineaArchivo = _archivo.readLine();
    _bordado->info.reset();

    while (_lineaArchivo.startsWith("#")){
        _bordado->info.comentario.append(_lineaArchivo);
        _lineaArchivo = _archivo.readLine();
    }

    bool isbke0 = _lineaArchivo.contains("BKE_00");

    while (!_lineaArchivo.startsWith("---") && !_archivo.atEnd()) {

        _lineaArchivo = _archivo.readLine();
        int inicio = _lineaArchivo.indexOf("=");
        QString dato = _lineaArchivo.mid(inicio + 1, _lineaArchivo.indexOf("\n") - inicio - 1);

        if(_lineaArchivo.startsWith("TITULO")){
            _bordado->info.titulo = dato;
        }
        else if(_lineaArchivo.startsWith("AUTORIA")){
            _bordado->info.autoria = dato;
        }
        else if(_lineaArchivo.startsWith("LICENCIA")){
            _bordado->info.licencia = dato;
        }
    }
    if (_archivo.atEnd()){
        qDebug() << "Error: No se ha encontrado la marca de inicio de instrucciones"
                    " \"---\" en el archivo de bordado.";
        return -1;
    }
    else if(isbke0 == true){
        return 0;
    }
    else {
        return -1; //Devolverá el número de versión BKE detectado en futuras versiones.
    }
}

void Bke::convertir(QString nuevoArchivo)
{
    QFile archivo(nuevoArchivo);

    if (archivo.open(QIODevice::WriteOnly | QIODevice::Text)){

        archivo.write("FORMATO=BKE_00\n");
        QByteArray qba("TITULO=");
        qba.append(_bordado->info.titulo);
        archivo.write(qba);
        qba = "\nAUTORIA=";
        qba.append(_bordado->info.autoria);
        archivo.write(qba);
        qba = "\nLICENCIA=";
        qba.append(_bordado->info.licencia);
        archivo.write(qba);

        for (int i = 0; i < _bordado->tam(); i++) {
            qba = "\n";
            Puntada p;
            p = _bordado->getPuntada(i);
            qba.append(QByteArray::number(p.x));
            qba.append(",");
            qba.append(QByteArray::number(p.y));
            qba.append(",");
            qba.append(QByteArray::number(p.flag));
            archivo.write(qba);
        }

        archivo.flush();
        archivo.close();
    }
    else {
        qDebug() << "No se pudo crear el archivo. ¿Directorio sin permisos?";
    }
}
