/********************************** LICENCIA **********************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
******************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mensaje.h"
#include "grabador.h"

#include <QPushButton>
#include <QFileDialog>
#include <QStatusBar>
#include <QListWidget>
#include <QMenu>
#include <QProgressBar>
#include <QCheckBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Bordado
    _bordado = new Bordado;
    //Trabajo
    _trabajo = new Trabajo(_bordado);
    //Simulación
    _simulacion = new Simulacion(_bordado);
    //Importador de bordados desde archivo
    _importador = new Importador(_bordado);
    //Groupbox de información de bordado
    _info = new GBInfo(ui->groupBoxInfo, _bordado);

    //Lienzo
    _escena = new CustomScene(this, _bordado);
    ui->graphicsView->setScene(_escena);

    //Comunicación con Arduino
    _puerto = new Serial;
    _config = new GBConfig(ui->groupBoxConfig, _puerto);
    _config->refrescaPuertos();
    _control = new GBControl(ui->groupBoxControl, _puerto);
    _gbTrabajo = new GBTrabajo(ui->gbTrabajo, _puerto, _trabajo, _bordado, _simulacion);

    //Inicializa otras variables
    _totalPuntadas = _puntadaActual = 0;

    //Hilo de simulación
    connect(_simulacion, SIGNAL(dibujarPuntada(int)), _escena, SLOT(pintarLinea(int)));
    connect(_simulacion, SIGNAL(setTiempo(QString)), this, SLOT(mostrarTiempo(QString)));
    connect(_simulacion, SIGNAL(actualizaProgreso(int)), this, SLOT(onActualizaProgreso(int)));

    connect(_gbTrabajo, SIGNAL(visualizarHasta(int)), _escena, SLOT(visualizarHasta(int)));
    connect(_gbTrabajo, SIGNAL(controlesActivos(bool)), SLOT(activarHerramientas(bool)));
    connect(_gbTrabajo, SIGNAL(aBordar(int,int)), _escena, SLOT(aBordar(int,int)));
    connect(_gbTrabajo, SIGNAL(reiniciarBordado()), _escena, SLOT(reiniciaBordado()));
    connect(_gbTrabajo, SIGNAL(progreso(int)), SLOT(onActualizaBarraYTiempo(int)));
    connect(_gbTrabajo, &GBTrabajo::reiniciarBordado, [&](){
        _puntadaActual = 0;
        mostrarInfoEspecial();
    });

    //Hilo de trabajo
    connect(_trabajo, SIGNAL(dibujarPuntada(int)), _escena, SLOT(pintarLinea(int)));
    connect(_trabajo,SIGNAL(setTiempo(QString)), this, SLOT(mostrarTiempo(QString)));
    connect(_trabajo, SIGNAL(enviar(QByteArray)), _puerto, SLOT(onEnviar(QByteArray)));
    connect(_trabajo, SIGNAL(actualizaProgreso(int)), this, SLOT(onActualizaProgreso(int)));

    //Escena de dibujo
    connect(_escena, SIGNAL(seHaPintado(int)), _simulacion, SLOT(onSeHaPintado()));
    connect(_escena, SIGNAL(seHaPintado(int)), _trabajo, SLOT(onSeHaPintado()));

    //Importador de bordados
    connect(_importador, SIGNAL(previsualiza()), this, SLOT(showPreview()));
    connect(_importador, SIGNAL(previsualiza()), _escena, SLOT(importarBordado()));
    connect(_importador, SIGNAL(previsualiza()), _info, SLOT(actualizaDimensiones()));
    connect(_importador, SIGNAL(previsualiza()), _gbTrabajo, SLOT(activarControles()));
    connect(_importador, SIGNAL(previsualiza()), _gbTrabajo, SLOT(cargarLista()));

    //Configuración serial
    connect(_config, SIGNAL(conectado(bool)), _control, SLOT(activarControles(bool)));
    connect(_config, SIGNAL(mostrarStatus(QString,int)), ui->statusBar, SLOT(showMessage(QString,int)));

    //Serial (Arduino)
    connect(_puerto, SIGNAL(setArduinoOk()), _trabajo, SLOT(setArduinoOk()));
    connect(_puerto, &Serial::continua, [&](){if (!_trabajo->isPausa())_trabajo->start();});

    //Barra de menú
    connect(ui->menuBar, SIGNAL(abrirArchivo()), SLOT(abrirArchivo()));
    connect(ui->menuBar, SIGNAL(abrirEjemplo(QString&)), SLOT(abrirEjemplo(QString&)));
    connect(ui->menuBar, SIGNAL(guardarComo()), SLOT(guardarComo()));

    //Herramientas de escena
    connect(ui->chkBoxBastidor, SIGNAL(toggled(bool)), _escena, SLOT(setBastidorVisible(bool)));
    connect(ui->cbBoxTamBastidor, SIGNAL(currentTextChanged(QString)), _escena, SLOT(pintarBastidor(QString)));
    connect(ui->pButtonRegla, SIGNAL(toggled(bool)), _escena, SLOT(setRejillaVisible(bool)));
    connect(ui->pButtonZoomMas, &QPushButton::clicked, [&](){ui->graphicsView->scale(1.15, 1.15);});
    connect(ui->pButtonZoomMenos, &QPushButton::clicked, [&](){ui->graphicsView->scale(0.85, 0.85);});
    connect(ui->pButtonMostrarMirilla, SIGNAL(clicked(bool)), _escena, SLOT(setMirillaVisible(bool)));
    connect(ui->pButtonRegla, SIGNAL(clicked(bool)), _escena, SLOT(setRejillaVisible(bool)));
    connect(ui->pButtonAjustar, &QPushButton::clicked, ui->graphicsView, &CustomGraphicsView::ajustarVista);

    //Herramientas de bordado
    connect(ui->pButtonRotarIzq, &QPushButton::clicked, [&](){_bordado->rotarIzq();});
    connect(ui->pButtonRotarDcha, &QPushButton::clicked, [&](){_bordado->rotarDcha();});
    connect(ui->pButtonVolteoHorizontal, &QPushButton::clicked, [&](){_bordado->volteoHorizontal();});
    connect(ui->pButtonVolteoVertical, &QPushButton::clicked, [&](){_bordado->volteoVertical();});

    //Bordado
    connect(_bordado, SIGNAL(transformacionFinalizada()), _escena, SLOT(importarBordado()));
    connect(_bordado, SIGNAL(transformacionFinalizada()), _gbTrabajo, SLOT(cargarLista()));
    connect(_bordado, SIGNAL(transformacionFinalizada()), this, SLOT(showPreview()));
    connect(_bordado, SIGNAL(transformacionFinalizada()), _info, SLOT(actualizaDimensiones()));

    #ifdef QT_DEBUG
        ///Importación rápida para pruebas
        QString ej(QDir::homePath()+"/.bordakenstein/bordados/Gallina");
        abrirEjemplo(ej);
    #endif
}

MainWindow::~MainWindow()
{
    delete ui;
}

/** --------------------------------------------------------------------------------
 * --------------------------------- Mainwindow ------------------------------------
 * ---------------------------------------------------------------------------------*/

void MainWindow::activarHerramientas(bool b)
{
    ui->groupBoxHerramientasBordado->setEnabled(b);
    ui->groupBoxControl->setEnabled(b);
    ui->groupBoxConfig->setEnabled(b);
}

void MainWindow::showPreview()
{
    _totalPuntadas = _bordado->tam() - 1;
    ui->progressBar->setValue(0);
    ui->groupBoxHerramientasBordado->setEnabled(true);
    setTitulo(_bordado->info.titulo);
}

/** --------------------------------------------------------------------------------
 * ------------------------------------ Info ---------------------------------------
 * ---------------------------------------------------------------------------------*/

//Actualiza información tras un caso especial
void MainWindow::onActualizaBarraYTiempo(int i)
{
    _puntadaActual = i;
    mostrarInfoEspecial();
}

void MainWindow::mostrarInfoEspecial()
{
    ui->progressBar->setValue(int(1000.0f / _totalPuntadas * _puntadaActual));
    mostrarTiempo("--:--:--");
}

//Durante el bordado o simulación se llama a este SLOT por cada puntada.
void MainWindow::onActualizaProgreso(int i)
{
    _puntadaActual = i;
    ui->progressBar->setValue(int(1000.0f / _totalPuntadas * _puntadaActual));
}

void MainWindow::mostrarTiempo(QString t)
{
    ui->labelTiempo->setText(t);
}

/** --------------------------------------------------------------------------------
 * --------------------------------- Archivo ---------------------------------------
 * ---------------------------------------------------------------------------------*/

void MainWindow::abrirArchivo()
{
    if (_comprobarTrabajoEnCurso()){
        _bordado->nombreArchivo = QFileDialog::getOpenFileName(this, "Abrir archivo", "/home",
                                                               "Formatos soportados (*.dst *.bke);;"
                                                               "Bordados Dst (*.dst);;"
                                                               "Bordakenstein extension Bke (*.bke)");
        if (!_bordado->nombreArchivo.isEmpty()){
            _abrirBordado();
        }
    }
}

void MainWindow::abrirEjemplo(QString &ejemplo)
{
    if (_comprobarTrabajoEnCurso()){
        _bordado->nombreArchivo = ejemplo;
        _abrirBordado();
    }
}

void MainWindow::guardarComo()
{
    Grabador * grabador = new Grabador(_bordado);
    QString nuevo = QFileDialog::getSaveFileName(this, "Guardar como", "/home",
                                                       "Bordakenstein extension (*.bke)");
    grabador->nuevoArchivo = nuevo;
    grabador->start();
}

void MainWindow::_abrirBordado()
{
    _escena->borrarDibujo();
    _gbTrabajo->borrarLista();

    _puntadaActual = _trabajo->contadorPuntadas = 0;
    _simulacion->detener();
    _importador->start();
}

bool MainWindow::_comprobarTrabajoEnCurso()
{
    if (_gbTrabajo->tareaEnCurso()){
        Mensaje::abrirConTrabajoEnCurso();
        return false;
    }
    else return true;
}

/** --------------------------------------------------------------------------------
 * --------------------------------- Eventos ---------------------------------------
 * ---------------------------------------------------------------------------------*/

//Previene el cierre del programa con un trabajo en ejecución.
void MainWindow::closeEvent(QCloseEvent *event)
{
    if (_gbTrabajo->tareaEnCurso()){
        if (Mensaje::salirConTrabajoEnEjecucion()) {
            if (_trabajo->trabajoEnCurso()){
                _trabajo->setPausa(true);
            }
            event->accept();
        }
        else event->ignore();
    }
    else event->accept();
}

//Shhhh!!! Es una sorpresa. No lo descubras ;)
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    static QByteArray code;
    code.append(event->text());
    if (code.length() > 3) code.remove(0, 1);
    if (code.endsWith("tux") && !(_trabajo->trabajoEnCurso() || _simulacion->simulacionEnCurso())){
        QSound::play(":/sonidos/fx_egg.wav");
        QString s(":/bordados/Love");
        abrirEjemplo(s);
    }
}

///Debería haber otra forma para ajustar el tamaño de gbtrabajo.ui con cualquier tamaño de mainwindow, igual que lo hace "promote to"
void MainWindow::resizeEvent(QResizeEvent *event)
{
    QRect r(ui->gbTrabajo->geometry());
    _gbTrabajo->setGeometry(0, 0, r.width(), r.height());
    event->accept();
}
