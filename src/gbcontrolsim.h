/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef GBCONTROLSIM_H
#define GBCONTROLSIM_H

#include <QGroupBox>
#include "simulacion.h"

namespace Ui {
class GBControlSim;
}

class GBControlSim : public QGroupBox
{
    Q_OBJECT

public:
    explicit GBControlSim(QWidget *parent, Simulacion *s, Bordado *b);
    ~GBControlSim();

private slots:
    void on_pButtonIniciarSimulacion_clicked();
    void on_pButtonAnterior_clicked();
    void on_pButtonSiguiente_clicked();
    void on_pButtonDetenerSimulacion_clicked();
    void on_pButtonPausarSimulacion_clicked();

signals:
    void activarModo();
    void pausar();
    void detener();
    void siguienteColor();
    void anteriorColor();

private:
    Ui::GBControlSim *ui;

    Simulacion *_simulacion;
    Bordado *_bordado;

    void botonesModoTrabajo(bool b);
};

#endif // GBCONTROLSIM_H
