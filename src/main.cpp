/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "mainwindow.h"
#include "acerca.h"

#include <QApplication>
//#include <QStyleFactory>
//#include <QDebug>

#define BORDAKENSTEIN_VERSION "v0.8.1.20190602"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("Bordakenstein");
    a.setApplicationVersion(BORDAKENSTEIN_VERSION);
    a.setOrganizationName("Solgrado");
    a.setOrganizationDomain("solgrado.gitlab.io");

#if defined(Q_OS_WIN)
    a.setStyle("fusion");
#endif

    /*QStyleFactory sf;
    QStringList sl = sf.keys();
    for(int i = 0; i < sl.size(); i++){
        qDebug() << sl[i];
    }*/

    MainWindow w;
    w.setWindowIcon(QPixmap(":/iconos/icono_logo.png"));
    w.showMaximized();
    Acerca::version = BORDAKENSTEIN_VERSION;

    return a.exec();
}
