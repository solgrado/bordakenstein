/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "gbcontrol.h"
#include "ui_gbcontrol.h"

GBControl::GBControl(QWidget *parent, Serial *s) :
    QGroupBox(parent),
    ui(new Ui::GBControl)
{
    ui->setupUi(this);
    this->setEnabled(false);

    _multiplicador = "1";
    _puerto = s;
}

GBControl::~GBControl()
{
    delete ui;
}

void GBControl::activarControles(bool b)
{
    this->setEnabled(b);
}

void GBControl::on_pButtonArriba_clicked()   {_puerto->write("U" + _multiplicador /*+ "\n"*/);}
void GBControl::on_pButtonAbajo_clicked()    {_puerto->write("D" + _multiplicador /*+ "\n"*/);}
void GBControl::on_pButtonIzquierda_clicked(){_puerto->write("L" + _multiplicador /*+ "\n"*/);}
void GBControl::on_pButtonDerecha_clicked()  {_puerto->write("R" + _multiplicador /*+ "\n"*/);}

void GBControl::on_pButtonIrAOrigen_clicked()
{
    _puerto->write("H");
    //_escena->setPosMirilla(QPoint(0, 0));
}

void GBControl::on_pButtonSetOrigen_clicked()
{
    _puerto->write("S");
    //_escena->setPosMirilla(QPoint(0, 0));
}

void GBControl::on_pButtonArrancarMotorAguja_clicked() {_puerto->write("M");} /// "\n" AL FINAL???
void GBControl::on_pButtonPararMotorAguja_clicked()    {_puerto->write("N");}
void GBControl::on_pButtonHabilitarMotores_clicked()   {_puerto->write("I");}
void GBControl::on_pButtonDeshabilitarMotores_clicked(){_puerto->write("J");} ///Debe haber constancia de que se han deshabilitado para que el resto del programa funcione correctamente

void GBControl::on_cbBoxMultip_currentIndexChanged(int index)
{
    switch(index){
    case 0: _multiplicador = "1"; break;
    case 1: _multiplicador = "10"; break;
    case 2: _multiplicador = "100"; break;
    case 3: _multiplicador = "1000"; break;
    }
}
