/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "barramenu.h"

#include <QUrl>
#include <QDesktopServices>
#include <QDir>
#include <QDebug>

BarraMenu::BarraMenu(QWidget *parent) : QMenuBar(parent)
{
    QMenu *menuArchivo = new QMenu(tr("Archivo"));
    menuArchivo->addAction(QIcon(":/iconos/document-open.png"), tr("Abrir"))->setData(0);
    menuArchivo->addAction(QIcon(":/iconos/guardar.png"), tr("Guardar como"))->setData(1);

    QMenu *menuBordados = new QMenu(tr("Bordados"));
    QMenu *menuEjemplos = new QMenu(tr("Ejemplos"));
    menuEjemplos->setIcon(QIcon(":/iconos/bordado.png"));
    menuBordados->addMenu(menuEjemplos);
    menuBordados->addAction(QIcon(":/iconos/repositorio.png"), tr("Repositorio"))->setData(1);

    QActionGroup* accionesEjemplos = new QActionGroup(menuEjemplos);

    // Sacar los bordados de los recursos y ponerlos en un directoio del usuario.

    QDir dirBordados(QDir::homePath()+"/.bordakenstein/bordados");

    if (dirBordados.exists()){
        dirBordados.setFilter(QDir::Files);
        dirBordados.setSorting(QDir::Name);
        QStringList listaBordados = dirBordados.entryList();

        for (int i = 0; i < listaBordados.size(); i++) {
            if (listaBordados.at(i) != QString("LICENSE") && listaBordados.at(i) != QString("README.md")){
                accionesEjemplos->addAction(menuEjemplos->addAction(QIcon(":/iconos/bordado.png"), listaBordados.at(i)))->setData(dirBordados.path() + "/" + listaBordados.at(i));
            }
        }
    }

    QMenu *menuInformacion = new QMenu(tr("Informacion"));
    menuInformacion->addAction(QIcon(":/iconos/ayuda.png"), tr("Manual de ayuda"))->setData(100);
    menuInformacion->addAction(QIcon(":/iconos/documentacion.png"), tr("Documentación de proyecto online"))->setData(101);
    menuInformacion->addAction(QIcon(":/iconos/info.png"), tr("Acerca de"))->setData(102);

    this->addMenu(menuArchivo);
    this->addMenu(menuBordados);
    this->addMenu(menuInformacion);

    connect(menuArchivo, SIGNAL(triggered(QAction*)), SLOT(ordenArchivo(QAction*)));
    connect(menuBordados, SIGNAL(triggered(QAction*)), SLOT(ordenAccion(QAction*)));
    connect(accionesEjemplos, SIGNAL(triggered(QAction*)), SLOT(ordenEjemplo(QAction*)));
    connect(menuInformacion, SIGNAL(triggered(QAction*)), SLOT(ordenAccion(QAction*)));
}

BarraMenu::~BarraMenu()
{
}

void BarraMenu::ordenAccion(QAction* accion)
{
    int i = accion->data().toInt();

    switch (i){
    case 1:   // Abrir url de repositorio
        QDesktopServices::openUrl(QUrl("https://gitlab.com/solgrado/bordakenstein-bordados.git"));
        break;
    case 100: // Abrir dialog de manual
        if (!Ayuda::estaInstanciada()){
            _ayuda = new Ayuda(this);
            _ayuda->setAttribute(Qt::WA_DeleteOnClose);
            _ayuda->show();
        }
        break;
    case 101: // Abrir url del manual
        QDesktopServices::openUrl(QUrl("https://solgrado.gitlab.io/bordakenstein-doc"));
        break;
    case 102: // Abrir dialog de acerca de.
        if (!Acerca::estaInstanciada()){
            _info = new Acerca(this);
            _info->setAttribute(Qt::WA_DeleteOnClose);
            _info->show();
        }
        break;
    default:
        break;
    }
}

void BarraMenu::ordenEjemplo(QAction *accion)
{
    QString a = accion->data().toString();
    emit abrirEjemplo(a);
}

void BarraMenu::ordenArchivo(QAction* accion)
{
    int i = accion->data().toInt();
    switch (i) {
    case 0: emit abrirArchivo(); break;
    case 1: emit guardarComo(); break;
    }
}

