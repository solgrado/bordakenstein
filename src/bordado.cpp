/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "bordado.h"

#define NORMAL  0 // puntada en (xx, yy)
#define JUMP    1 // mover a(xx, yy)
#define TRIM    2 // trim + mueve a(xx, yy)
#define STOP    4 // pausa por cambio de hilo
#define SEQUIN  8 // sequin
#define END    16 // fin del programa

Bordado::Bordado(QObject *parent) : QObject(parent)
{
    reset();
    nombreArchivo.clear();
}

QLine Bordado::lineaPuntada(int index)
{
    if (index < 2){
        return QLine(0, 0, -_lPuntadas.at(index).x, -_lPuntadas.at(index).y);
    }
    else {
        return QLine(-_lPuntadas.at(index - 1).x, -_lPuntadas.at(index - 1).y,
                     -_lPuntadas.at(index).x, -_lPuntadas.at(index).y);
    }
}

QPoint Bordado::puntoPuntada(int index)
{
    return QPoint(lineaPuntada(index).x2(), lineaPuntada(index).y2());
}

//puntada = negro, salto = azul
QColor Bordado::colorPuntada(int index){
    if (index > 1){
        if (_lPuntadas.at(index - 1).flag == NORMAL && _lPuntadas.at(index).flag == NORMAL){
            return QColor(0, 0, 0);
        }
        else if (_lPuntadas.at(index - 1).flag == JUMP && _lPuntadas.at(index).flag == JUMP){
            return QColor(0, 0, 255);
        }
        else { //pendiente de definir otros casos
            return QColor(0, 0, 255);
        }
    }
    else {
        return QColor(0, 0, 255);
    }
}

QColor Bordado::colorLista(int index)
{
    switch(_lPuntadas.at(index).flag){
    case 0:  return QColor("#b0ffb0");
    case 1:  return QColor("#b0b0ff");
    case 2:  return QColor("#ffb0b0");
    case 4:  return QColor("#b0ffff");
    case 8:  return QColor("#ffffb0");
    case 16: return QColor("#ffb0ff");
    default: return QColor("#ffffff");
    }
}

QString Bordado::textoPuntada(int index)
{
    QString texto;
    switch(_lPuntadas.at(index).flag){
    case 0:  texto = "Puntada"; break;
    case 1:  texto = "Salto"; break;
    case 2:  texto = "DESCONOCIDO"; break;
    case 4:  texto = "Cambio Color"; break;
    case 8:  texto = "DESCONOCIDO"; break;
    case 16: texto = "Fin"; break;
    }
    QLine l = lineaPuntada(index);
    return QString::number(index) + "\t" + QString::number(l.x2()) + "\t"
            + QString::number(l.y2()) + "\t" + texto;
}

QByteArray Bordado::textoAPuerto(int index)
{
    unsigned short flag = _lPuntadas.at(index).flag;

    QLine l = lineaPuntada(index);
    QByteArray qba;
    qba.clear();
    qba.append('X' + QString::number(l.x2()) +
               'Y' + QString::number(l.y2()) +
               'F' + QByteArray::number(flag) + '\t');

    ////////////////////URGE MODIFICARLO EN ARDUINO///////////////////////

    /* Sistema para ahorrar bytes de comunicación. Debido a los nuevos desplazamientos libres
     * a cualquier número de puntada ya no se puede utilizar así.
    if (index > 0){
        if (_lPuntadas.at(index).flag != _lPuntadas.at(index - 1).flag){
            qba.append('F' + QByteArray::number(flag) + '\t');
        }
    }
    else {
        qba.append('F' + QByteArray::number(flag) + '\t');
    }*/

    return qba;
}

void Bordado::volteoHorizontal()
{
    for (int i = 0; i < _lPuntadas.size(); i++){
        _lPuntadas[i].x = -_lPuntadas[i].x;
    }
    emit transformacionFinalizada();
}

void Bordado::volteoVertical()
{
    for (int i = 0; i < _lPuntadas.size(); i++){
        _lPuntadas[i].y = -_lPuntadas[i].y;
    }
    emit transformacionFinalizada();
}

void Bordado::rotarIzq()
{
    int h;
    for (int i = 0; i < _lPuntadas.size(); i++){
        h = _lPuntadas[i].x;
        _lPuntadas[i].x = -_lPuntadas[i].y;
        _lPuntadas[i].y = h;
    }
    emit transformacionFinalizada();
}

void Bordado::rotarDcha()
{
    int h;
    for (int i = 0; i < _lPuntadas.size(); i++){
        h = _lPuntadas[i].y;
        _lPuntadas[i].y = -_lPuntadas[i].x;
        _lPuntadas[i].x = h;
    }
    emit transformacionFinalizada();
}
