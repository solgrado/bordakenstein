/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "dst.h"
#include <QFile>
#include <QDataStream>
#include <QDebug>

Dst::Dst(Bordado *bordado)
{
    _bordado = bordado;
}

quint8 Dst::_decodeFlag(unsigned char b2)
{
    quint8 returnCode = 0;
    if(b2 == 0xF3) return END;
    if(b2 & 0x80) returnCode = JUMP;
    if(b2 & 0x40) returnCode = STOP;
    return returnCode;
}

int Dst::importar()
{
    QFile archivo(_bordado->nombreArchivo);
    archivo.open(QIODevice::ReadOnly);
    QDataStream inDato(&archivo);

    _bordado->reset();
    _bordado->info.reset();

    char temp[512];
    inDato.readRawData(temp, 512);
    QByteArray qbaCabecera;
    qbaCabecera.append(temp, 512);

    _bordado->info.titulo = QString::fromStdString(qbaCabecera.mid(3, 16).toStdString());
    _bordado->info.formato = "dst";

    quint8 byte[3];

    while(inDato.status() == QDataStream::Ok)
    {
        Puntada p;
        inDato >> byte[0] >> byte[1] >> byte[2];

        if (byte[0] & 0x01) p.x += 1;
        if (byte[0] & 0x02) p.x -= 1;
        if (byte[0] & 0x04) p.x += 9;
        if (byte[0] & 0x08) p.x -= 9;
        if (byte[0] & 0x80) p.y += 1;
        if (byte[0] & 0x40) p.y -= 1;
        if (byte[0] & 0x20) p.y += 9;
        if (byte[0] & 0x10) p.y -= 9;
        if (byte[1] & 0x01) p.x += 3;
        if (byte[1] & 0x02) p.x -= 3;
        if (byte[1] & 0x04) p.x += 27;
        if (byte[1] & 0x08) p.x -= 27;
        if (byte[1] & 0x80) p.y += 3;
        if (byte[1] & 0x40) p.y -= 3;
        if (byte[1] & 0x20) p.y += 27;
        if (byte[1] & 0x10) p.y -= 27;
        if (byte[2] & 0x04) p.x += 81;
        if (byte[2] & 0x08) p.x -= 81;
        if (byte[2] & 0x20) p.y += 81;
        if (byte[2] & 0x10) p.y -= 81;

        p.flag = _decodeFlag(byte[2]);

        // Convertir en coordenadas absolutas
        if (_bordado->tam() > 0){
            p.x += _bordado->lastX();
            p.y += _bordado->lastY();
        }

        if (p.flag == STOP || p.flag == END) {
            _bordado->addCambioColor(_bordado->tam());
        }

        _bordado->addPuntada(p);

        if (p.flag == END) {
            break;
        }
    }

    archivo.close();

    return 0;
}


