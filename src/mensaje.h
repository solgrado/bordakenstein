/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef MENSAJE_H
#define MENSAJE_H

#include <QSound>
#include <QMessageBox>

/*
 * Grupo de métodos estáticos para llamadas a mensajes
*/

class Mensaje
{
public:
    Mensaje();

    //Advierte de un cierre del programa sin finalizar un trabajo o simulación.
    static bool salirConTrabajoEnEjecucion();

    //Advierte de una importación de bordado sin finalizar un trabajo o simulación.
    static void abrirConTrabajoEnCurso();

    //Avisa de un cambio de hilo.
    static bool cambioDeHilo();

    //Avisa de un intento de bordado sin haberse conectado con la bordadora.
    static void puertoNoConfigurado();

    //Avisa de que un trabajo o simulación ha llegado a su fin.
    static bool trabajoFinalizado();

    //Advierte un error de conexión por falta de permisos.
    static void errorPermisos();

    //Confirma que se quiere desplazar directamente a una instrucción desde la lista.
    static bool confirmarDesplazamiento();
};

#endif // MENSAJE_H
