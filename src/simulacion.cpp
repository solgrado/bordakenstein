/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "simulacion.h"

Simulacion::Simulacion(Bordado *b, QObject *parent) : QThread(parent)
{
    _contadorPuntadas = 0;
    _pausa = true;
    _intervaloSim = 50;
    _bordado = b;
}

void Simulacion::setPausa(bool b)
{
    _pausa = b;
    _tiempo.restart();
}

void Simulacion::detener()
{
    setPausa(true);
    _contadorPuntadas = 0;
}

void Simulacion::_getTiempo()
{
    // Actualiza el tiempo restante cada diez puntadas.
    if (_contadorPuntadas % 10 == 0){
        int t = _tiempo.msecsTo(QTime::currentTime()) * ((_bordado->tam() - _contadorPuntadas) / 10);
        QTime temp(0, 0, 0, 0);
        temp = temp.addMSecs(t);
        _tiempo.restart();

        emit setTiempo(temp.toString("hh:mm:ss"));
    }
}

void Simulacion::run()
{
    _dibujoCompleto = true;
    setPausa(false);

    while(!_pausa && _contadorPuntadas < _bordado->tam()){
        if(_dibujoCompleto)
        {
            _dibujoCompleto = false;
            emit dibujarPuntada(_contadorPuntadas);
            emit actualizaProgreso(_contadorPuntadas);

            switch (_bordado->getPuntada(_contadorPuntadas).flag) {
            case STOP:
                setPausa(true);
                emit cambiarHilo();
                break;
            case END:
                setPausa(true);
                emit final();
                break;
            default:
                break;
            }
            _contadorPuntadas++;
            _getTiempo();
            msleep(ulong(_intervaloSim));
        }
    }
}

void Simulacion::onSeHaPintado()
{
    _dibujoCompleto = true;
}
