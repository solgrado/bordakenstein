/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef GBCONFIG_H
#define GBCONFIG_H

#include <QGroupBox>
#include <QSerialPortInfo>

#include "serial.h"

namespace Ui {
class GBConfig;
}

class GBConfig : public QGroupBox
{
    Q_OBJECT

public:
    explicit GBConfig(QWidget *parent, Serial *s);
    ~GBConfig();

public slots:
    //Actualiza la lista de puertos con los periféricos detectados
    void refrescaPuertos();

private slots:
    void on_pButtonEnviarParametrosMotores_clicked();
    void on_pButtonConectar_toggled(bool checked);
    void mostrarInfo();

signals:
    void conectado(bool);
    void mostrarStatus(QString, int);

private:
    Ui::GBConfig *ui;

    QList<QSerialPortInfo> _listaPuertos;
    Serial *_puerto;

    void importarConfiguracion();
    void guardarConfiguracion();
};

#endif // GBCONFIG_H
