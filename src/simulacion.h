/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef SIMULACION_H
#define SIMULACION_H

#include <QThread>
#include <QTime>
#include <QObject>

#include "bordado.h"

class Simulacion : public QThread
{
    Q_OBJECT
public:
    explicit Simulacion(Bordado *b, QObject *parent = nullptr);

    //Devuelve si la simulación se encuentra en estado de pausa
    bool isPausa()const {return _pausa;}
    void setPuntadaActual(const int p) {_contadorPuntadas = p;}
    bool simulacionEnCurso()const {return !_pausa;}

protected:
    void run() override;

signals:
    void actualizaProgreso(int);
    void cambiarHilo();
    void dibujarPuntada(int);
    void setTiempo(QString);
    void final();

public slots:
    void onSeHaPintado();
    void setPausa(bool b);
    void setIntervaloSim(int t) {_intervaloSim = t;}
    void detener();

private:
    QTime _tiempo;
    Bordado *_bordado;

    int _contadorPuntadas;
    bool _dibujoCompleto;
    bool _pausa;
    int _intervaloSim;

    void _getTiempo();
};

#endif // SIMULACION_H
