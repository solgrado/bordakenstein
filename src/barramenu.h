/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef MENU_H
#define MENU_H

#include "ayuda.h"
#include "acerca.h"

#include <QWidget>
#include <QMenuBar>


class BarraMenu : public QMenuBar
{
    Q_OBJECT

public:
    explicit BarraMenu(QWidget *parent = nullptr);
    ~BarraMenu();

signals:
    void abrirArchivo();
    void abrirEjemplo(QString &ejemplo);
    void guardarComo();

private slots:
    void ordenAccion(QAction* accion);
    void ordenEjemplo(QAction * accion);
    void ordenArchivo(QAction* accion);

private:
    Acerca *_info;
    Ayuda *_ayuda;
};

#endif // MENU_H
