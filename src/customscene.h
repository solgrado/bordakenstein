/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef CUSTOMSCENE_H
#define CUSTOMSCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsSvgItem>

class Bordado;

class CustomScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit CustomScene(QObject *parent, Bordado *b);

    //Dibuja la rejilla en la escena
    void dibujarRejilla(const int tamBastidor);

    //Mueve la mirilla a la posición "punto"
    void setPosMirilla(const QPoint punto);

    //Elimina el dibujo de la escena (sólo las líneas del bordado)
    void borrarDibujo();

    //Pinta de color rojo las puntadas que están en el rango indicado
    void pendienteBordar(const int inicio, const int fin);

    //Pinta de color negro las puntadas que están en el rango indicado
    void estaBordado(const int inicio, const int fin);


public slots:
    //Pinta el dibujo con los colores correspondientes a un trabajo situado en la puntada "n"
    void visualizarHasta(int n);

    //Devuelve la escena con la imagen del bordado al punto de partida
    void reiniciaBordado();

    //Pinta en color resaltado las puntadas que quedan por hacer con el color de hilo actual
    void aBordar(const int inicio, const int fin);

    //Cambia la apariencia de cada puntada durante el trabajo o simulación
    void pintarLinea(int i);

    //Añade una nueva línea a la lista de líneas.
    void importarBordado();

    //Muestra u oculta la mirilla
    void setMirillaVisible(bool b);

    //Muestra u oculta la rejilla
    void setRejillaVisible(bool b);

    //Muestra u oculta el bastidor
    void setBastidorVisible(bool b);

    //Redimensiona el bastidor y el límite.
    void pintarBastidor(QString tamBastidor);

signals:
    void seHaPintado(int);

private:
    QGraphicsEllipseItem *_bastidor;
    QGraphicsEllipseItem *_limite;
    QGraphicsSvgItem *_mirilla;
    QList<QGraphicsLineItem *> _rejilla;
    bool _rejillaEsVisible;
    Bordado *_bordado;
    QList<QGraphicsLineItem *> _listaItemLineas;
};

#endif // CUSTOMSCENE_H
