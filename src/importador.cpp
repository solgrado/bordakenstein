/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/


#include "importador.h"
#include "bke.h"
#include "dst.h"

#include <QDataStream>
#include <QDebug>

Importador::Importador(Bordado *b, QObject *parent) : QThread(parent)
{
    _bordado = b;
}

void Importador::run()
{
    if (_bordado->nombreArchivo.endsWith(".dst", Qt::CaseInsensitive)){
        Dst dst(_bordado);
        if (dst.importar() == 0){
            emit previsualiza();
        }
        else {
            qDebug() << "Error al importar archivo dst.";
        }
    }
    else {
        Bke bke(_bordado);
        int error = bke.importar();
        if (error == 0){
            emit previsualiza();
        }
        else {
            qDebug() << "Error al intentar importar archivo como bke.";
        }
    }
}

