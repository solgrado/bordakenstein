/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "mensaje.h"

Mensaje::Mensaje()
{
}

bool Mensaje::salirConTrabajoEnEjecucion()
{
    QSound::play(":/sonidos/fx_error.wav");
    QMessageBox::StandardButton boton;
    boton = QMessageBox::information(nullptr, "Salir",
                                     "Hay un trabajo o simulación en ejecución.\n"
                                     "¿Deseas interrumpirlo y salir?",
                                     QMessageBox::Ok | QMessageBox::Cancel);

    return (boton == QMessageBox::Ok);
}

void Mensaje::abrirConTrabajoEnCurso()
{
    QMessageBox msgbox(QMessageBox::Information, "Trabajo en curso",
                       "Se encuentra un trabajo de bordado o simulación en ejecución.\n"
                       "Detenlo antes de abrir otro bordado.",
                       QMessageBox::Ok);

    QSound::play(":/sonidos/fx_error.wav");
    msgbox.exec();
}

bool Mensaje::cambioDeHilo()
{
    QMessageBox msgbox(QMessageBox::Information, "Cambio de hilo",
                       "Bordakenstein permanece a la espera\n"
                       "del cambio de hilo correspondiente.\n"
                       "Dale a aceptar cuando esté listo.",
                       QMessageBox::Ok);

    QSound::play(":/sonidos/fx_aviso.wav");
    return msgbox.exec();
}

void Mensaje::puertoNoConfigurado()
{
    QMessageBox msgbox(QMessageBox::Information, "Puerto no configurado",
                       "Para iniciar un bordado es necesario conectarse\n"
                       " con la bordadora desde la pestaña \"Configuración\".",
                       QMessageBox::Ok);

    QSound::play(":/sonidos/fx_error.wav");
    msgbox.exec();
}

bool Mensaje::trabajoFinalizado()
{
    QMessageBox msgbox(QMessageBox::Information, "Final",
                       "El bordado ha llegado a su fin.",
                       QMessageBox::Ok);

    QSound::play(":/sonidos/fx_fin.wav");
    return msgbox.exec();
}

void Mensaje::errorPermisos()
{
    QMessageBox msgbox(QMessageBox::Critical, "Error conexión",
                       " La comunicación con el puerto ha devuelto el siguiente error:\n\n"
                       " \"Se produjo un error al intentar abrir un dispositivo"
                       " ya abierto por otro proceso o el usuario no tiene"
                       " suficientes permisos y credenciales para abrirlo.\" \n\n"
                       " Para obtener los permisos añade tu usuario"
                       " al grupo que corresponde según el caso: \n"
                       " Ubuntu:  sudo usermod -a -G dialout <TuNombreDeUsuario> \n"
                       " Manjaro: sudo usermod -a -G uucp <TuNombreDeUsuario>");
    msgbox.addButton("Cerrar", QMessageBox::RejectRole);
    QSound::play(":/sonidos/fx_error.wav");
    msgbox.exec();
}

bool Mensaje::confirmarDesplazamiento()
{
    QMessageBox::StandardButton boton;
    QSound::play(":/sonidos/fx_aviso2.wav");
    boton = QMessageBox::information(nullptr, "Confirmar desplazamiento",
                                     "¿Estás seguro/a de querer desplazarte a la nueva instrucción?.\n",
                                     QMessageBox::Ok | QMessageBox::Cancel);

    return (boton == QMessageBox::Ok);
}
