/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef AYUDA_H
#define AYUDA_H

#include <QDialog>

namespace Ui {
class Ayuda;
}

class Ayuda : public QDialog
{
    Q_OBJECT

public:
    explicit Ayuda(QWidget *parent = nullptr);
    ~Ayuda();

    //Devuelve "true" en el caso de que ya exista una instancia de Ayuda
    static bool estaInstanciada();

private:
    Ui::Ayuda *ui;

    static bool _hayInstancia;
};

#endif // AYUDA_H
