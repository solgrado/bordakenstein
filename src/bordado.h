/****************************************************************************
**
** Copyright (C) 2017-2019 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef PUNTADAS_H
#define PUNTADAS_H

#include <QObject>
#include <QVector>
#include <QList>
#include <QLine>
#include <QColor>
#include <QByteArray>

#define NORMAL  0 // puntada en (xx, yy)
#define JUMP    1 // mover a(xx, yy)
#define TRIM    2 // trim + mueve a(xx, yy)
#define STOP    4 // pausa por cambio de hilo
#define SEQUIN  8 // sequin
#define END    16 // fin del programa

#define LIBRE  32 // ajeno al bordado. Pensado para movimientos libres del bastidor

//Estructura básica de una puntada
struct Puntada{
    //Coordenada X
    int x;
    //Coordenada Y
    int y;
    //Acción a efectuar sobre las coordenadas
    unsigned short flag;
    //Constructor
    Puntada(): x(0), y(0), flag(0)
    {}
};

struct Info{
    QString formato;
    QString titulo;
    QString autoria;
    QString licencia;
    QByteArray comentario;

    Info(): formato(""), titulo(""), autoria(""), licencia(""), comentario(""){}

    void reset(){
        formato = "";
        titulo = "";
        autoria = "";
        licencia = "";
        comentario = "";
    }
};

class Bordado : public QObject
{
    Q_OBJECT
public:
    explicit Bordado(QObject *parent = nullptr);

    //Devuelve la puntada del bordado con índice "index"
    Puntada getPuntada(const int index) const {return _lPuntadas.at(index);}

    //x, y, flag de la última puntada añadida a la lista
    int lastX() const {return _lPuntadas.last().x;}
    int lastY() const {return _lPuntadas.last().y;}
    unsigned short lastFlag() const {return _lPuntadas.last().flag;}

    //Añadir una puntada a la lista de puntadas
    void addPuntada(const Puntada p) {_lPuntadas.append(p);}

    //Vaciar la lista de puntadas
    void reset() {
        _lPuntadas.clear();
        resetCambioColor();
    }

    //Devuelve una línea desde la anterior puntada hasta la puntada actual
    QLine lineaPuntada(const int index);

    //Punto exacto de la puntada
    QPoint puntoPuntada(const int index);

    //Color para la representación en graphicsview
    QColor colorPuntada(const int index);

    //Color para la lista
    QColor colorLista(const int index);

    //Texto de la accion (Puntada, Salto, Cambio Color...)
    QString textoPuntada(const int index);

    //Cantidad de puntadas
    int tam() const {return _lPuntadas.size();}

    //Vaciar la lista de cambios de color
    void resetCambioColor() {
        _cambioColor.clear();
        _cambioColor.append(0);
    }

    //Número de puntada en la que termina el color al que pertenece la puntada
    int getLimiteColor(const int numPuntada) {
        for (int i = 1; i < _cambioColor.size(); i++){
            if (numPuntada <= _cambioColor[i]){
                return _cambioColor[i];
            }
        }
        return -1;
    }

    //Número de color al que corresponde la puntada
    int getNumColor(const int numPuntada){
        for (int i = 0; i < _cambioColor.size(); i++){
            if (numPuntada <= _cambioColor[i]){
                return i;
            }
        }
        return -1;
    }

    //Añadir un cambio de color (numero de puntada) a la lista de cambios
    void addCambioColor(const int i) {_cambioColor.append(i);}

    //Retorna un número de puntada para un cambio de color
    int puntadaFinDeColor(const int nColor) const {return _cambioColor.at(nColor);}
    int puntadaInicioDeColor(const int nColor) const {
        if (nColor == 1) {return 0;}
        else return _cambioColor.at(nColor - 1) + 1;
    }

    //Cantidad de cambios de color. Se restan 2 porque el vector incluye el dato de
    //inicio (0) y el final (totalPuntadas) que no son cambios como tales.
    int numCambios() const {return _cambioColor.size() - 2;}

    // Comando listo para enviar a arduino
    QByteArray textoAPuerto(int index);

    //Devuelve acción a realizar en un punto del bordado
    unsigned short getFlag(int index){
        return _lPuntadas.at(index).flag;
    }

    /// --- Herramientas de bordado --- ///
    
    //Invierte el dibujo en el eje X
    void volteoHorizontal();

    //Invierte el dibujo en el eje Y
    void volteoVertical();

    //Rota el dibujo 90 grados en sentido antihorario
    void rotarIzq();

    //Rota el dibujo 90 grados en sentido horario
    void rotarDcha();

    QString nombreArchivo;
    Info info;

signals:
    void transformacionFinalizada();

private:
    QList<Puntada> _lPuntadas;
    QVector<int> _cambioColor;
};

#endif // PUNTADAS_H
