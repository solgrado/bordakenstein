/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "gbconfig.h"
#include "ui_gbconfig.h"
#include "mensaje.h"

#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QSpinBox>

GBConfig::GBConfig(QWidget *parent, Serial *s) :
    QGroupBox(parent),
    ui(new Ui::GBConfig)
{
    ui->setupUi(this);

    _puerto = s;
    ui->pButtonConectar->setEnabled(false);

    importarConfiguracion();

    connect(ui->pButtonRefrescaPuertos, SIGNAL(clicked(bool)), this, SLOT(refrescaPuertos()));
    connect(ui->cbBoxPuertos, SIGNAL(currentIndexChanged(int)), this, SLOT(mostrarInfo()));
}

GBConfig::~GBConfig()
{
    delete ui;
}

void GBConfig::importarConfiguracion()
{
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "Solgrado", "Bordakenstein");
    settings.beginGroup("Config");
    ui->spinBoxMaxVelMotX->setValue(settings.value("MaxVelMotX", 1500).toInt());
    ui->spinBoxMaxVelMotY->setValue(settings.value("MaxVelMotY", 1500).toInt());
    ui->spinBoxAcelMotX->setValue(settings.value("AcelMotX", 2000).toInt());
    ui->spinBoxAcelMotY->setValue(settings.value("AcelMotY", 2000).toInt());
    settings.endGroup();
}

void GBConfig::guardarConfiguracion()
{
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "Solgrado", "Bordakenstein");
    settings.beginGroup("Config");
    settings.setValue("MaxVelMotX", QString::number(ui->spinBoxMaxVelMotX->value()));
    settings.setValue("MaxVelMotY", QString::number(ui->spinBoxMaxVelMotY->value()));
    settings.setValue("AcelMotX", QString::number(ui->spinBoxAcelMotX->value()));
    settings.setValue("AcelMotY", QString::number(ui->spinBoxAcelMotY->value()));
    settings.endGroup();

    emit mostrarStatus("La configuración se ha guardado en " + settings.fileName(), 10000);
}

void GBConfig::on_pButtonConectar_toggled(bool checked)
{
    if (checked){
        int i = _puerto->conectar(ui->cbBoxPuertos->currentText());

        if (i < 0){
            ui->pButtonConectar->setText("Desconectar");
            emit mostrarStatus("Se ha establecido conexión con " + ui->cbBoxPuertos->currentText(), 10000);
        }
        else {
            ui->pButtonConectar->setChecked(false);
            if (i == 2){
                Mensaje::errorPermisos();
            }
            emit mostrarStatus("Error de conexión " + QString::number(i), 10000);
        }
    }
    else{
        _puerto->desconectar();
        ui->pButtonConectar->setText("Conectar");
    }

    ui->groupBoxConfigMotores->setEnabled(checked);
    emit conectado(checked);
}

void GBConfig::mostrarInfo()
{
    int i = ui->cbBoxPuertos->currentIndex();

    if (i >= 0){
        ui->labelDescripcion->setText(_listaPuertos.at(i).description());
        ui->labelManufacturado->setText(_listaPuertos.at(i).manufacturer());
        ui->labelIDProducto->setText(QString::number(_listaPuertos.at(i).productIdentifier()));
        ui->labelNumSerie->setText(_listaPuertos.at(i).serialNumber());
        ui->labelDirSistema->setText(_listaPuertos.at(i).systemLocation());
        ui->labelIDFrabricante->setText(QString::number(_listaPuertos.at(i).vendorIdentifier()));
    }
}

void GBConfig::refrescaPuertos()
{
    ui->cbBoxPuertos->clear();
    _listaPuertos = QSerialPortInfo::availablePorts();
    for (int i = 0; i < _listaPuertos.size(); i++){
        ui->cbBoxPuertos->addItem(_listaPuertos.at(i).portName());
    }
    ui->pButtonConectar->setEnabled(ui->cbBoxPuertos->count() > 0);

    mostrarInfo();
}

void GBConfig::on_pButtonEnviarParametrosMotores_clicked()
{
    QByteArray qba;
    qba.append("A");
    qba.append(QString::number(ui->spinBoxMaxVelMotX->value()) + '\n');
    qba.append(QString::number(ui->spinBoxAcelMotX->value()) + '\n');
    qba.append(QString::number(ui->spinBoxMaxVelMotY->value()) + '\n');
    qba.append(QString::number(ui->spinBoxAcelMotY->value()) + '\n');

    _puerto->write(qba);

    guardarConfiguracion();
}
