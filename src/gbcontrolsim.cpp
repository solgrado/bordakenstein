/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#include "gbcontrolsim.h"
#include "ui_gbcontrolsim.h"

#include "bordado.h"
#include <qdebug.h>

GBControlSim::GBControlSim(QWidget *parent, Simulacion *s, Bordado *b) :
    QGroupBox(parent),
    ui(new Ui::GBControlSim)
{
    ui->setupUi(this);
    _bordado = b;
    _simulacion = s;

    connect(ui->sliderTiempo, SIGNAL(valueChanged(int)), _simulacion, SLOT(setIntervaloSim(int)));
}

GBControlSim::~GBControlSim()
{
    delete ui;
}

void GBControlSim::on_pButtonIniciarSimulacion_clicked()
{
    if (!_bordado->nombreArchivo.isEmpty()){
        emit activarModo();

        botonesModoTrabajo(true);

        _simulacion->start();
    }
}

void GBControlSim::on_pButtonAnterior_clicked()
{
    emit anteriorColor();
}


void GBControlSim::on_pButtonSiguiente_clicked()
{
    emit siguienteColor();
}

void GBControlSim::on_pButtonDetenerSimulacion_clicked()
{
    _simulacion->detener();

    botonesModoTrabajo(false);

    emit detener();
}

void GBControlSim::on_pButtonPausarSimulacion_clicked()
{
    _simulacion->setPausa(true);

    botonesModoTrabajo(false);

    emit pausar();
}

void GBControlSim::botonesModoTrabajo(bool b)
{
    ui->pButtonIniciarSimulacion->setDisabled(b);
    ui->pButtonDetenerSimulacion->setDisabled(b);
    ui->pButtonPausarSimulacion->setEnabled(b);
    ui->pButtonAnterior->setDisabled(b);
    ui->pButtonSiguiente->setDisabled(b);
}
