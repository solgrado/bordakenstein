/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef GBCONTROL_H
#define GBCONTROL_H

#include <QGroupBox>

#include "serial.h"

namespace Ui {
class GBControl;
}

class GBControl : public QGroupBox
{
    Q_OBJECT

public:
    explicit GBControl(QWidget *parent, Serial *s);
    ~GBControl();

public slots:
    //Activa o desactiva el groupbox de controles
    void activarControles(bool b);

private slots:
    void on_pButtonArriba_clicked();
    void on_pButtonAbajo_clicked();
    void on_pButtonIzquierda_clicked();
    void on_pButtonDerecha_clicked();
    void on_cbBoxMultip_currentIndexChanged(int index);
    void on_pButtonSetOrigen_clicked();
    void on_pButtonIrAOrigen_clicked();
    void on_pButtonArrancarMotorAguja_clicked();
    void on_pButtonPararMotorAguja_clicked();
    void on_pButtonHabilitarMotores_clicked();
    void on_pButtonDeshabilitarMotores_clicked();

private:
    Ui::GBControl *ui;

    Serial *_puerto;
    QByteArray _multiplicador;
};

#endif // GBCONTROL_H
