/****************************************************************************
**
** Copyright (C) 2017-2018 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Bordakenstein.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef GBTRABAJO_H
#define GBTRABAJO_H

#include <QGroupBox>
#include "trabajo.h"
#include "serial.h"
#include "gbcontrolsim.h"

namespace Ui {
class GBTrabajo;
}

class GBTrabajo : public QGroupBox
{
    Q_OBJECT

public:
    explicit GBTrabajo(QWidget *parent,
                       Serial *puerto,
                       Trabajo *hilo,
                       Bordado *bordado,
                       Simulacion *simulacion);
    ~GBTrabajo();

    //Ejecuta un cambio de hilo en la escena
    void saltarAColor();

    //Intrucciones para detener un trabajo
    void detenerTrabajo();

    //Tareas comunes al detener un trabajo o una simulación
    void detener();

    //Cambio de hilo durante un trabajo
    void cambiarHilo();

    //Tareas comunes de cambio de hilo en trabajo o simulación
    void sumaUnHilo();

    //Borra la lista (tableWidget) de instrucciones del bordado
    void borrarLista();

    //Devuelve si un trabajo o simulación está en ejecución
    bool tareaEnCurso();

public slots:
    void desplazarHastaAqui();
    void visualizarHastaAqui();
    void activarControles();
    void on_pButtonIniciar_clicked();
    void submenu(const QPoint &pos);
    void cargarLista();
    void activarModoSim();
    void cambiarHiloSim();
    void detenerSim();
    void saltoAColorSiguiente();
    void saltoAColorAnterior();
    void actualizarProgreso(int i);

signals:
    void reiniciarBordado();
    void aBordar(int, int);
    void controlesActivos(bool);
    void visualizarHasta(int);
    void mostrarInfo();
    void progreso(int);

private slots:
    void finalizar();
    void on_tabWidgetBordar_currentChanged(int index);
    void on_pButtonPausar_clicked();

private:
    Ui::GBTrabajo *ui;

    Serial *_puerto;
    Trabajo *_trabajo;
    Bordado *_bordado;
    Simulacion *_simulacion;
    GBControlSim *_gbcontrolsim;

    quint8 _numColor;
    int _totalPuntadas;
    int _puntadaActual;
    bool _trabajoEnCurso;

    void botonesModoTrabajo(bool b);
};

#endif // GBTRABAJO_H
