#-------------------------------------------------
#
# Project created by QtCreator 2017-04-03T15:15:14
#
#-------------------------------------------------

QT       += core gui
QT       += serialport
QT       += multimedia
QT       += svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =    Bordakenstein
TEMPLATE =  app

SOURCES += \
            src/acerca.cpp \
            src/ayuda.cpp \
            src/barramenu.cpp \
            src/bke.cpp \
            src/bordado.cpp \
            src/customgraphicsview.cpp \
            src/customscene.cpp \
            src/dst.cpp \
            src/gbconfig.cpp \
            src/gbcontrol.cpp \
            src/gbcontrolsim.cpp \
            src/gbinfo.cpp \
            src/gbtrabajo.cpp \
            src/grabador.cpp \
            src/importador.cpp \
            src/main.cpp \
            src/mainwindow.cpp \
            src/masinfo.cpp \
            src/mensaje.cpp \
            src/serial.cpp \
            src/simulacion.cpp \
            src/trabajo.cpp

HEADERS  += \
            src/acerca.h \
            src/ayuda.h \
            src/barramenu.h \
            src/bke.h \
            src/bordado.h \
            src/customgraphicsview.h \
            src/customscene.h \
            src/dst.h \
            src/gbconfig.h \
            src/gbcontrol.h \
            src/gbcontrolsim.h \
            src/gbinfo.h \
            src/gbtrabajo.h \
            src/grabador.h \
            src/importador.h \
            src/mainwindow.h \
            src/masinfo.h \
            src/mensaje.h \
            src/serial.h \
            src/simulacion.h \
            src/trabajo.h

FORMS    += \
            src/acerca.ui \
            src/ayuda.ui \
            src/gbconfig.ui \
            src/gbcontrol.ui \
            src/gbcontrolsim.ui \
            src/gbinfo.ui \
            src/gbtrabajo.ui \
            src/mainwindow.ui \
            src/masinfo.ui

RESOURCES += \
            res/res.qrc \

QMAKE_CXXFLAGS += -std=gnu++17

#Copiar directorio de ejemplos de bordado (si ha sido descargada) a la carpeta de usuario.
exists($$PWD/bordados){
    !exists($$~/.bordakenstein){system(mkdir $$~/.bordakenstein)}
    copydata.commands = $(COPY_DIR) $$PWD/bordados $$~/.bordakenstein
    first.depends = $(first) copydata
    export(first.depends)
    export(copydata.commands)
    QMAKE_EXTRA_TARGETS += first copydata
}
